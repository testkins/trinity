# Trinity

Documents relating to [Bastl's Trinity](https://www.bastl-instruments.com/instruments/trinity/)

This repository contains:

 - EagleCAD schematic

See Blog: [Unholy Trinity](https://gr33nonline.wordpress.com/2018/10/16/unholy-trinity/)

